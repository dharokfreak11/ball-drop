
package net.sam.game;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Engine extends JFrame{
    
    
    JFrame frame;
    int width, height;
    
    public static void main(String[] args){
        new Engine();
    }

    public Engine(){
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame = this;
        this.width = (int) d.getWidth();
        this.height = (int) d.getHeight();
        frame.setTitle("Game - By Samuel A. Horton");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(d);
        FPSHandler fpsHandler = new FPSHandler();
        Thread thread = new Thread(fpsHandler);
        thread.start();
        GameScreen screen = new GameScreen(this);
        frame.add(screen);
        frame.setVisible(true);
    }
    
    public int getScreenWidth(){
        return width;
    }
    
    public int getScreenHeight(){
        return height;
    }
    
    
}
