

package net.sam.game.entities;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sam.game.GameScreen;
import net.sam.game.mechanics.Gravity;

public class Entity implements Runnable{

	double x, y, width, height;
	int altitude;
	double dx = 0, dy = 0, dt = .2;
	double energyLoss = .95;
	double gravityValue = 0;
	Color color;
	boolean gravity;
	public static ArrayList<Entity> entities = new ArrayList<Entity>();


	public Entity(int x, int y, int width, int height, boolean gravity) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.gravity = gravity;
		this.gravityValue = Gravity.getGravityValue();
		this.altitude = 100;
		entities.add(this);
		Thread fall = new Thread(this);
		fall.start();
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double d) {
		this.y = d;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public boolean hasGravity(){
		return gravity;
	}

	public void setGravity(boolean value){
		this.gravity = value;
	}

	public void fall(){
		this.gravityValue = Gravity.getGravityValue();
		Plane gamePlane = GameScreen.gamePlane;
		if(getY() > 680){
			setY(680);
			dy *= energyLoss;
			dy = -dy;
		}else{
			dy += gravityValue * dt;
			setY(getY() + (dy * dt + .5 * gravityValue * dt * dt));
		}

	}

	public void move(int left, int right, int up, int down){
		if(left > 0){
			for(int i = 0; i < left; i++){
				setX(getX() - 5);
			}
		}else if(right > 0){
			for(int i = 0; i < right; i++){
				setX(getX() + 5);
			}
		}else if(up > 0){
			for(int i = 0; i < up; i++){
				setY(getY() - 5);
			}
		}else if(down > 0){
			for(int i = 0; i < down; i++){
				setY(getY() + 5);
			}
		}
	}

	public Color getColor(){
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void run() {
		while(hasGravity()){
			try {
				Thread.sleep(10);
				fall();
			} catch (InterruptedException ex) {
				Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

}
