

package net.sam.game.entities;

import java.awt.Graphics;
import java.util.ArrayList;

public class Plane extends Entity{

    public static ArrayList<Plane> planes = new ArrayList<Plane>();
    
    public Plane(int x, int y, int width, int height) {
        super(x, y, width, height, false);
        planes.add(this);
    }

    @Override
    public double getX() {
        return x; 
    }

    @Override
    public double getY() {
        return y;
    }
    
    @Override
    public double getWidth() {
        return width;
    }
    
    @Override
    public double getHeight() {
        return height;
    }
    
    public void draw(Graphics g){
        g.fillRect((int)getX(), (int)getY(), (int)getWidth(), (int)getHeight());
    }
    
    public boolean contains(int yPos){
        if(yPos >= 700 && yPos <= 800){
            return true;
        }else{
            return false;
        }
    }
    
}
