

package net.sam.game.entities;

import java.awt.Color;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Ball extends Entity{
    
    public static ArrayList<Ball> balls = new ArrayList<Ball>();
    
    int time;
    int velocity = 0;
    
    public Ball(int x, int y, int width, int height){
        super(x, y, width, height, true);
        balls.add(this);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    
    

    public double getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
    
    
}
