

package net.sam.game.mechanics;

import net.sam.game.entities.Entity;
import java.util.ArrayList;
import net.sam.game.GameScreen;
import net.sam.game.entities.Plane;

public class Gravity {
    
    public static ArrayList<Entity> entities = new ArrayList<Entity>();
    
    private static final int GRAVITY_VALUE = 10;
    
    public static int getGravityValue(){
        return GRAVITY_VALUE;
    }
    
    public static Plane getGamePlane(){
        return GameScreen.getGamePlane();
    }
    
}
