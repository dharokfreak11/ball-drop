package net.sam.game;

public class FPSHandler implements Runnable{

	public static int fps;
	private boolean running = true;
	
	@Override
	public void run(){
		long lastFrame = System.currentTimeMillis();
		int frames = 0;
		while(running){
			frames++;
			if(System.currentTimeMillis() - 1000 >= lastFrame){
				fps = frames;
				frames = 0;
				lastFrame = System.currentTimeMillis();
			}
			try{
				Thread.sleep(2);
				
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

	public int getFps(){
		return fps;
	}
	
	
	
}
