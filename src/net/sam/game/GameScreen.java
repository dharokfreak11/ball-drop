/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.sam.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JPanel;
import net.sam.game.entities.Ball;
import net.sam.game.entities.Plane;


public class GameScreen extends JPanel implements KeyListener, MouseListener, MouseMotionListener {

    boolean isClicked = false;
    int x = 0, y = 0;
    Engine engine;
    GameScreen screen;
    Color background, planeColor;
    public static Plane gamePlane;
    
    public GameScreen(Engine engine) {
        this.engine = engine;
        this.screen = this;
        engine.addKeyListener(screen);
        this.addMouseListener(screen);
        this.addMouseMotionListener(screen);
        repaintScreen();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.clearRect(0, 0, engine.getScreenWidth(), engine.getScreenHeight());
        g.setColor(background);
        g.fillRect(0, 0, engine.getScreenWidth(), engine.getScreenHeight());
        g.setColor(Color.red);
        g.drawString("X: " + x + ", Y: " + y, 100, 100);
        g.drawString("FPS: " + FPSHandler.fps, 100, 120);
        gamePlane = new Plane(0, 700, screen.getWidth(), 100);
        g.setColor(getPlaneColor());
        gamePlane.draw(g);
        ArrayList<Ball> bombs = (ArrayList<Ball>) Ball.balls.clone();
        for(Ball b : bombs){
            g.setColor(b.getColor());
            g.fillRoundRect((int)b.getX(), (int)b.getY(), (int)b.getWidth(), (int)b.getHeight(), 200, 200);
        }
        
    }

    public void repaintScreen(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run(){
                while(Thread.interrupted() == false){
                    engine.frame.repaint();
                }
            }
        });
        thread.start();
    }

    

    @Override
    public void mouseClicked(MouseEvent e) {
        if(isClicked){
        	this.isClicked = false;
        }else{
        	this.isClicked = true;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.x = e.getX();
        this.y = e.getY();
        if(isClicked){
        	Ball b = new Ball(x, y, 50, 50);
            Random r = new Random();
            b.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_SPACE){
            Random r = new Random();
            setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

    public static Plane getGamePlane() {
        return gamePlane;
    }

    public static void setGamePlane(Plane plane) {
        gamePlane = plane;
    }
    
    public void setColor(Color color){
        this.background = color;
    }
    
    
    public Color getColor(){
        return background;
    }
    
    public void setPlaneColor(Color color){
        this.planeColor = color;
    }
    
    
    public Color getPlaneColor(){
        return planeColor;
    }
    
}
